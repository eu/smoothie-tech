<?php declare(strict_types=1);

namespace inc;

class View
{
    public const __VIEW__ = "../App/View/";

    public function __construct()
    {
    }

    public function render(array $v): void
    {
	/*
	 * View
	 * $v['view'] = index.phtml
	 */
	if (!isset($v['view']))
	    $v['view'] = '404.html';

	extract($v);

	ob_start();
	require_once self::__VIEW__.(file_exists(self::__VIEW__.$view) ? $view : '404.html');
	ob_end_flush();
    }
}
