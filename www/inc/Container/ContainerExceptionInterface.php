<?php declare(strict_types=1);

namespace inc\Container;

/**
 * Base interface representing a generic exception in a container.
 */
interface ContainerExceptionInterface
{
}

