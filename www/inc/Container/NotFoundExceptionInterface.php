<?php declare(strict_types=1);

namespace inc\Container;

use inc\Container\ContainerExceptionInterface;

/**
 * No entry was found in the container.
 */
interface NotFoundExceptionInterface extends ContainerExceptionInterface
{
}
