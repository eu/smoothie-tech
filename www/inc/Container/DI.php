<?php declare(strict_types=1);

namespace inc\Container;

use inc\Container\NotFoundException;

class DI implements ContainerInterface
{
    private $container = [];

    public function get(string $className): ?object
    {
	try {
	    return new $className();
	} catch (\ArgumentCountError $e) {
	    $ref = new \ReflectionClass($className);
	    $parameters = $ref->getConstructor()->getParameters();
	    $params = [];
	    foreach ($parameters as $p) {
		$type = (string) $p->getType();
		if (!$this->has($type)) {
		    throw new NotFoundException($type);
		}
		$params[] = $this->get($type);

	    }
	    return $ref->newInstanceArgs($params);
	}
    }

    public function set(string $className): bool
    {
	if (!isset($this->container[$className])) {
	    $this->container[$className] = function () { return new $className(); };
	    return true;
	}
	return false;
    }

    public function has(string $className): bool
    {
	return isset($this->container[$className]);
    }
}

