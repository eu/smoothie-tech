<?php declare(strict_types=1);

namespace inc\Container;

use inc\Container\NotFoundExceptionInterface;

class NotFoundException extends \Exception implements NotFoundExceptionInterface
{
    public function errorMessage(): string
    {
        return "Class " . $this->getMessage() . " not found!\n";
    }
}
