<?php declare(strict_types=1);

namespace inc;

class Request
{
    public function getUri(): string
    {
	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	return preg_replace(['#^'.ROOT.'#', '#/$#'], '', $uri);
    }
}
