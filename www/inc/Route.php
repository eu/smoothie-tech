<?php declare(strict_types=1);

namespace inc;

class Route
{
    private const __ROUTES__ = "../cfg/";

    protected $routes;
    protected $uri;

    public function __construct(Request $request)
    {
	$this->routes = require_once self::__ROUTES__."routes.php";
	$this->uri = $request->getUri();
    }

    use \inc\Sanitize;

    public function get(): array
    {
	foreach ($this->routes as $route) {
	    /*
	     * route[0] = 'test/(\d+)'
	     * route[1] = 'MainController@test'
	     * argv = [ int from (\d+) ]
	     */
	    if (preg_match('#'.$route[0].'#', $this->uri, $argv)) {
		array_shift($argv);
		foreach ($argv as $key => $arg) {
		    if (!strncmp($arg, (string)(int) $arg, strlen($arg)))
			$argv[$key] = (int) $arg;
		    else
			$argv[$key] = $this->s($arg);
		}
		$call = explode('@', $route[1]);
		if (empty($call[0]) || empty($call[1]))
		    throw new \Exception("No route!");
		break;
	    }
	}
	return [
	    'controller' => $call[0],
	    'method' => $call[1],
	    'argv' => $argv
	];
    }
}
