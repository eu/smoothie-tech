<?php declare(strict_types=1);

define('PREFIX', '');
define('BASE_DIR', __DIR__ . '/../');

spl_autoload_register(function ($class) {
    $len = strlen(PREFIX);

    if (strncmp(PREFIX, $class, $len) !== 0)
        return;

    $relative_class = substr($class, $len);

    $file = BASE_DIR . str_replace('\\', '/', $relative_class) . '.php';

    if (file_exists($file))
	require_once $file;
    else
	die("Autoload error: \"$file\" not found!\n");
});
