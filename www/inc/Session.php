<?php declare(strict_types=1);

namespace inc;

defined('SESSID') || define('SESSID', 'DEFAULT_SESSID');

class Session
{
    private $id;

    public function __construct()
    {
	session_name(SESSID);
	session_start();
	$this->id = session_id();
    }

    public function __destruct()
    {
	session_unset();
    }

    public function get(string $name)
    {
	return $_SESSION[$name] ?? null;
    }

    public function set(string $name, $value): void
    {
	$_SESSION[$name] = $value;
    }
}
