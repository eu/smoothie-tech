<?php declare(strict_types=1);

namespace inc;

trait Sanitize
{
    function s($s, int $quotes = ENT_QUOTES): string
    {
	return htmlentities(trim("$s"), $quotes, 'UTF-8');
    }
}
