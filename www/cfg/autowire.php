<?php declare(strict_types=1);

define('AUTOWIRE', [

    'inc\Session',
    'inc\Request',
    'inc\Route',
    'inc\View'

]);
