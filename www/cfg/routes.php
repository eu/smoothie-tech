<?php

return [
    [ 'hi$',            'MainController@greeting' ],
    [ 'hi/(\d+)$',      'MainController@greeting' ],
    [ 'hi/(\S+)/(\d+)$','MainController@personalGreeting' ],
    [ 'hi/(\S+)$',      'MainController@personalGreeting' ],
    [ '',               'MainController@home' ],
];
