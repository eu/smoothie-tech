<?php declare(strict_types=1);

namespace App;

require_once "../inc/autoload.php";
require_once "../cfg/app.php";
require_once "../cfg/autowire.php";

use inc\Container\DI;

try {
    $di = new DI();

    foreach (AUTOWIRE as $className)
        $di->set("$className");

    $sess  = $di->get("inc\Session");
    $route = $di->get("inc\Route");
    $view  = $di->get("inc\View");

    $route = $route->get();
    $di->set('App\Controller\\' . $route['controller']);
    $controller = $di->get('App\Controller\\' . $route['controller']);
    $v = call_user_func_array([ $controller, $route['method'] ], $route['argv']);

    $view->render($v);
} catch (\Exception $e) {
    echo $e->errorMessage();
}
