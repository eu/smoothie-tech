<?php declare(strict_types=1);

namespace App\Controller;

class MainController
{
    public function home(): array
    {
	return [ "view" => "home.phtml" ];
    }

    public function greeting(int $n = 1): array
    {
	return [
	    "view" => "greeting.phtml",
	    "n" => $n
	];
    }

    public function personalGreeting(string $name, int $n = 1): array
    {
	return [
	    "view" => "greeting.phtml",
	    "name" => $name,
	    "n" => $n
	];
    }
}
