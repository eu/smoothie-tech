### Study of 'smoothie' technologies

###### Invention of the wheel for educational purposes

Autoload сlasses by namespaces
```sh
inc/autoload.php
```
Dependency Injection Container with autowire and lazyload
```sh
cfg/autowire.php
inc/Container/DI.php
```
Router
```sh
cfg/routes.php
inc/Route.php
```
#### Usage
```sh
./dock up   #Need docker-compose to run
wget -q -O- http://localhost:8080
wget -q -O- http://localhost:8080/hi
wget -q -O- http://localhost:8080/hi/Darling/3
```
##### Сleanup
```sh
./dock down   # Delete created containers ~600MB
```
